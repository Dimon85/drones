package com.nemtsov.drones.api;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.nemtsov.drones.dto.ExceptionDto;
import com.nemtsov.drones.dto.MedicationDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * Medication API.
 *
 * @author nemtsov free11@list.ru
 */
@Tag(
        name = "MedicationApi"
)
@RequestMapping("api/medications")
public interface MedicationApi {

    @Operation(summary = "Add medication")
    @ApiResponses(
            {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Medication has been added",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = MedicationDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    )
            }
    )
    @PostMapping(
            value = "",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<MedicationDto> addMedication(@Valid @RequestBody MedicationDto medicationDto);

    @Operation(summary = "Get all medications")
    @ApiResponses(
            {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Get all medications",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = MedicationDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    )
            }
    )
    @GetMapping(
            value = "",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<List<MedicationDto>> getAllMedications();

    @Operation(summary = "Delete medication")
    @ApiResponses(
            {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Medication has been deleted",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = Boolean.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Not found",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    )
            }
    )
    @DeleteMapping(
            value = "/{medicationId}",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<String> deleteMedication(@PathVariable Long medicationId);
}
