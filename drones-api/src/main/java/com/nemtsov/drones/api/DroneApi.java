package com.nemtsov.drones.api;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.nemtsov.drones.dto.DroneDto;
import com.nemtsov.drones.dto.ExceptionDto;
import com.nemtsov.drones.dto.MedicationDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * Drone API.
 *
 * @author nemtsov free11@list.ru
 */
@Tag(
        name = "DroneApi"
)
@RequestMapping("api/drones")
public interface DroneApi {

    @Operation(summary = "Register drone")
    @ApiResponses(
            {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Drone registered",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = DroneDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    )
            }
    )
    @PostMapping(
            value = "",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<DroneDto> registerDrone(@Valid @RequestBody DroneDto droneDto);

    @Operation(summary = "Drones available for loading")
    @ApiResponses(
            {
                    @ApiResponse(
                            responseCode = "200",
                            description = "List of drones",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = DroneDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    )
            }
    )
    @GetMapping(
            value = "/available",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<List<DroneDto>> availableDrones();

    @Operation(summary = "Get battery level for a given drone")
    @ApiResponses(
            {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Battery level",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = String.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Not found",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    )
            }
    )
    @GetMapping(
            value = "/{droneId}/battery",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<Short> getBatteryLevel(@PathVariable Long droneId);

    @Operation(summary = "Check loaded medication items for a given drone")
    @ApiResponses(
            {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Medication",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = MedicationDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Not found",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    )
            }
    )
    @GetMapping(
            value = "/{droneId}/medication",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<List<MedicationDto>> getMedications(@PathVariable Long droneId);

    @Operation(summary = "Load drone with medication items")
    @ApiResponses(
            {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Success",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = String.class))
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Not found",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    )
            }
    )
    @PostMapping(
            value = "/{droneId}/load",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<String> loadMedication(@PathVariable Long droneId, @RequestBody List<Long> medicationIds);

    @Operation(summary = "Get all drones")
    @ApiResponses(
            {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Get all drones",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = DroneDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    )
            }
    )
    @GetMapping(
            value = "",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<List<DroneDto>> getAllActiveDrones();

    @Operation(summary = "Delete drone")
    @ApiResponses(
            {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Drone has been deleted",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = Boolean.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Not found",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = @Content(mediaType = "application/json",
                                               schema = @Schema(implementation = ExceptionDto.class))
                    )
            }
    )
    @DeleteMapping(
            value = "/{droneId}",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<String> deleteDrone(@PathVariable Long droneId);
}
