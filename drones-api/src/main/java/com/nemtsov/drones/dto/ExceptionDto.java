package com.nemtsov.drones.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

/**
 * DTO for application exceptions.
 *
 * @author nemtsov free11@list.ru
 */
@Schema(name = "Exception")
@Getter
@Builder
public class ExceptionDto {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 3418684455882011748L;

    /**
     * Message.
     */
    @Schema(
            title = "Message",
            example = "Text message"
    )
    private String message;

    /**
     * Timestamp.
     */
    @Schema(
            title = "Timestamp",
            example = "2022-12-07T22:05:42.158+00:00"
    )
    private LocalDateTime timestamp;

    /**
     * Status code.
     */
    @Schema(
            title = "Status code",
            example = "200"
    )
    private Integer status;
}
