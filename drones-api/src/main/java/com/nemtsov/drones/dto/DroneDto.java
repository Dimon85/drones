package com.nemtsov.drones.dto;

import com.nemtsov.drones.enums.DroneModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Drone DTO.
 *
 * @author nemtsov free11@list.ru
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Drone")
public class DroneDto {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1547075057110636982L;

    /**
     * Id.
     */
    @Schema(
            title = "Unique identifier",
            example = "1"
    )
    private Long id;

    /**
     * Serial number.
     */
    @Schema(
            title = "Serial number",
            example = "7836482136493295792454"
    )
    @Size(max = 100, message = "Weight must be no longer than 100 characters")
    @NotBlank(message = "Serial number must be not empty")
    private String serialNumber;

    /**
     * Drone model.
     */
    @Schema(
            title = "Drone model",
            example = "LIGHTWEIGHT"
    )
    @NotNull(message = "Model must not be null")
    private DroneModel model;

    /**
     * Weight.
     */
    @Schema(
            title = "Weight",
            example = "100"
    )
    @Max(value = 500, message = "Weight must be less than or equal to 500")
    @NotNull(message = "Weight must not be null")
    private Short weight;

    /**
     * Charge level.
     */
    @Schema(
            title = "Charge level",
            example = "100"
    )
    @Max(value = 100, message = "Weight must be less than or equal to 100")
    private Short chargeLevel;

    /**
     * State.
     */
    @Schema(
            title = "State",
            example = "IDLE"
    )
    private String state;

    /**
     * Active status.
     */
    @Schema(
            title = "Active",
            example = "true"
    )
    private Boolean active;
}
