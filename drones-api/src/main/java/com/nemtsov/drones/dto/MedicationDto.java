package com.nemtsov.drones.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * Medication DTO.
 *
 * @author nemtsov free11@list.ru
 */
@Data
@Builder
@Validated
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Medication")
public class MedicationDto {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -4630608271574415039L;

    /**
     * Id.
     */
    @Schema(
            title = "Unique identifier",
            example = "1"
    )
    private Long id;

    /**
     * Medication name.
     */
    @Schema(
            title = "Name",
            example = "Medication"
    )
    @NotBlank(message = "Medication name must not be empty")
    @Pattern(regexp = "^[a-zA-Z0-9_-]*$", message = "Name must contain only letters, numbers, ‘-‘, ‘_’")
    private String name;

    /**
     * Medication weight.
     */
    @Schema(
            title = "Weight",
            example = "100"
    )
    private Short weight;

    /**
     * Medication code.
     */
    @Schema(
            title = "Medication code",
            example = "MED_1"
    )
    @Pattern(regexp = "^[A-Z0-9_]*$", message = "Code must contain only upper case letters, numbers, ‘_’")
    private String code;

    /**
     * Image URL.
     */
    @Schema(
            title = "Image URL"
    )
    private String imageUrl;

    /**
     * Active status.
     */
    @Schema(
            title = "Active",
            example = "true"
    )
    private Boolean active;
}
