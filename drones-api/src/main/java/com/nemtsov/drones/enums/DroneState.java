package com.nemtsov.drones.enums;

/**
 * Drone state.
 *
 * @author nemtsov free11@list.ru
 */
public enum DroneState {

    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING
}
