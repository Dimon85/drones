package com.nemtsov.drones.enums;

/**
 * Drone model.
 *
 * @author nemtsov free11@list.ru
 */
public enum DroneModel {

    LIGHTWEIGHT,
    MIDDLEWEIGHT,
    CRUISERWEIGHT,
    HEAVYWEIGHT
}
