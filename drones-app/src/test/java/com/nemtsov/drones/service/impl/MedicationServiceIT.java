package com.nemtsov.drones.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import com.nemtsov.drones.dto.MedicationDto;
import com.nemtsov.drones.entity.Medication;
import com.nemtsov.drones.repository.MedicationRepository;
import com.nemtsov.drones.service.MedicationService;
import com.nemtsov.drones.support.PostgreSQLContainerTestConfiguration;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

/**
 * Integration tests for {@link MedicationService}
 *
 * @author nemtsov free11@list.ru
 */
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
@ContextConfiguration(classes = {PostgreSQLContainerTestConfiguration.class})
@ComponentScan(
        basePackages = {
                "com.nemtsov.drones.service",
                "com.nemtsov.drones.mapper",
                "com.nemtsov.drones.provider"
        }
)
class MedicationServiceIT {

    @Autowired
    MedicationService medicationService;

    @Autowired
    MedicationRepository medicationRepository;

    private static final String MEDICATION_NAME_1 = "Medication_1";
    private static final Short MEDICATION_WEIGHT_1 = 100;
    private static final String MEDICATION_CODE_1 = "Med_1";
    private static final String MEDICATION_IMAGE_URL_1 = "Med_URL_1";
    private static final String MEDICATION_NAME_2 = "Medication_2";
    private static final Short MEDICATION_WEIGHT_2 = 200;
    private static final String MEDICATION_CODE_2 = "Med_2";
    private static final String MEDICATION_IMAGE_URL_2 = "Med_URL_2";
    private static final String MEDICATION_NAME_3 = "Medication_3";
    private static final Short MEDICATION_WEIGHT_3 = 300;
    private static final String MEDICATION_CODE_3 = "Med_3";
    private static final String MEDICATION_IMAGE_URL_3 = "Med_URL_3";

    @Test
    @DisplayName("Add medication")
    void addMedication() {

        MedicationDto medicationDto = MedicationDto.builder()
                                                   .name(MEDICATION_NAME_1)
                                                   .weight(MEDICATION_WEIGHT_1)
                                                   .code(MEDICATION_CODE_1)
                                                   .imageUrl(MEDICATION_IMAGE_URL_1)
                                                   .build();

        final MedicationDto resultMedicationDto = medicationService.addMedication(medicationDto);

        assertNotNull(resultMedicationDto);
        assertNotNull(resultMedicationDto.getId());
        assertThat(resultMedicationDto).usingRecursiveComparison().ignoringFields("id").isEqualTo(medicationDto);
    }

    @Test
    @DisplayName("Get all active medications")
    void getAllMedications() {

        Medication medication_1 = Medication.builder()
                                            .name(MEDICATION_NAME_1)
                                            .weight(MEDICATION_WEIGHT_1)
                                            .code(MEDICATION_CODE_1)
                                            .imageUrl(MEDICATION_IMAGE_URL_1)
                                            .active(true)
                                            .build();

        medicationRepository.save(medication_1);

        Medication medication_2 = Medication.builder()
                                            .name(MEDICATION_NAME_2)
                                            .weight(MEDICATION_WEIGHT_2)
                                            .code(MEDICATION_CODE_2)
                                            .imageUrl(MEDICATION_IMAGE_URL_2)
                                            .active(true)
                                            .build();

        medicationRepository.save(medication_2);

        Medication medication_3 = Medication.builder()
                                            .name(MEDICATION_NAME_3)
                                            .weight(MEDICATION_WEIGHT_3)
                                            .code(MEDICATION_CODE_3)
                                            .imageUrl(MEDICATION_IMAGE_URL_3)
                                            .active(false)
                                            .build();

        medicationRepository.save(medication_3);

        final List<MedicationDto> medicationDtoList = medicationService.getAllActiveMedications();
        assertNotNull(medicationDtoList);
        assertEquals(medicationDtoList.size(), 2);
    }

    @Test
    @DisplayName("Delete medication")
    void deleteMedication() {

        Medication medication_1 = Medication.builder()
                                            .name(MEDICATION_NAME_1)
                                            .weight(MEDICATION_WEIGHT_1)
                                            .code(MEDICATION_CODE_1)
                                            .imageUrl(MEDICATION_IMAGE_URL_1)
                                            .active(true)
                                            .build();

        final Medication medication = medicationRepository.save(medication_1);

        medicationService.deleteMedication(medication.getId());
        final Optional<Medication> resultMedication = medicationRepository.findById(medication.getId());

        assertThat(resultMedication).isNotEmpty();
        assertEquals(resultMedication.get().getActive(), false);
    }
}