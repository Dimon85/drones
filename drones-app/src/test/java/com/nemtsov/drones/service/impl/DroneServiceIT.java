package com.nemtsov.drones.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import com.nemtsov.drones.dto.DroneDto;
import com.nemtsov.drones.dto.MedicationDto;
import com.nemtsov.drones.entity.Drone;
import com.nemtsov.drones.entity.DroneMedication;
import com.nemtsov.drones.entity.Medication;
import com.nemtsov.drones.enums.DroneModel;
import com.nemtsov.drones.enums.DroneState;
import com.nemtsov.drones.exception.BadRequestException;
import com.nemtsov.drones.repository.DroneMedicationRepository;
import com.nemtsov.drones.repository.DroneRepository;
import com.nemtsov.drones.repository.MedicationRepository;
import com.nemtsov.drones.service.DroneService;
import com.nemtsov.drones.support.PostgreSQLContainerTestConfiguration;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Integration tests for {@link DroneService}
 *
 * @author nemtsov free11@list.ru
 */
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
@ContextConfiguration(classes = {PostgreSQLContainerTestConfiguration.class})
@ComponentScan(
        basePackages = {
                "com.nemtsov.drones.service",
                "com.nemtsov.drones.mapper",
                "com.nemtsov.drones.provider"
        }
)
class DroneServiceIT {

    @Autowired
    DroneService droneService;

    @Autowired
    DroneRepository droneRepository;

    @Autowired
    DroneMedicationRepository droneMedicationRepository;

    @Autowired
    MedicationRepository medicationRepository;

    private static final String DRONE_SERIAL_NUMBER = "7836482136493295792454";
    private static final DroneModel DRONE_MODEL = DroneModel.LIGHTWEIGHT;
    private static final Short DRONE_WEIGHT = 300;
    private static final String DRONE_SERIAL_NUMBER_2 = "6402830475493295792432";
    private static final DroneModel DRONE_MODEL_2 = DroneModel.MIDDLEWEIGHT;
    private static final Short DRONE_WEIGHT_2 = 200;
    private static final String DRONE_SERIAL_NUMBER_3 = "5462830475493295792432";
    private static final DroneModel DRONE_MODEL_3 = DroneModel.MIDDLEWEIGHT;
    private static final Short DRONE_WEIGHT_3 = 200;
    private static final Short HIGH_CHARGE_LEVEL = 100;
    private static final Short LOW_CHARGE_LEVEL = 15;
    private static final String MEDICATION_NAME_1 = "Medication_1";
    private static final Short MEDICATION_WEIGHT_1 = 100;
    private static final String MEDICATION_NAME_2 = "Medication_2";
    private static final Short MEDICATION_WEIGHT_2 = 200;

    @Test
    @DisplayName("Register drone")
    void registerDrone() {

        DroneDto droneDto = DroneDto.builder()
                                    .serialNumber(DRONE_SERIAL_NUMBER)
                                    .model(DRONE_MODEL)
                                    .weight(DRONE_WEIGHT)
                                    .build();

        final DroneDto resultDroneDto = droneService.registerDrone(droneDto);

        assertNotNull(resultDroneDto);
        assertNotNull(resultDroneDto.getId());
        assertThat(resultDroneDto).usingRecursiveComparison().ignoringFields("id", "chargeLevel", "state")
                                  .isEqualTo(droneDto);
        assertEquals(resultDroneDto.getChargeLevel(), (short) 100);
        assertEquals(resultDroneDto.getState(), DroneState.IDLE.name());
    }

    @Test
    @DisplayName("Get available drones")
    void availableDrones() {

        Drone drone_1 = Drone.builder()
                             .serialNumber(DRONE_SERIAL_NUMBER)
                             .model(DRONE_MODEL)
                             .weight(DRONE_WEIGHT)
                             .chargeLevel(HIGH_CHARGE_LEVEL)
                             .state(DroneState.IDLE)
                             .active(true)
                             .build();

        droneRepository.save(drone_1);

        Drone drone_2 = Drone.builder()
                             .serialNumber(DRONE_SERIAL_NUMBER_2)
                             .model(DRONE_MODEL_2)
                             .weight(DRONE_WEIGHT_2)
                             .chargeLevel(LOW_CHARGE_LEVEL)
                             .state(DroneState.IDLE)
                             .active(true)
                             .build();

        droneRepository.save(drone_2);

        Drone drone_3 = Drone.builder()
                             .serialNumber(DRONE_SERIAL_NUMBER_3)
                             .model(DRONE_MODEL_3)
                             .weight(DRONE_WEIGHT_3)
                             .chargeLevel(HIGH_CHARGE_LEVEL)
                             .state(DroneState.LOADING)
                             .active(true)
                             .build();

        droneRepository.save(drone_3);

        Drone drone_4 = Drone.builder()
                             .serialNumber(DRONE_SERIAL_NUMBER_3)
                             .model(DRONE_MODEL_3)
                             .weight(DRONE_WEIGHT_3)
                             .chargeLevel(HIGH_CHARGE_LEVEL)
                             .state(DroneState.IDLE)
                             .chargeLevel(HIGH_CHARGE_LEVEL)
                             .state(DroneState.IDLE)
                             .active(false)
                             .build();

        droneRepository.save(drone_4);

        final List<DroneDto> droneDtoList = droneService.availableDrones();
        assertNotNull(droneDtoList);
        assertEquals(droneDtoList.size(), 1);
    }

    @Test
    @DisplayName("Get battery level")
    void getBatteryLevel() {

        Drone drone = Drone.builder()
                           .serialNumber(DRONE_SERIAL_NUMBER)
                           .model(DRONE_MODEL)
                           .weight(DRONE_WEIGHT)
                           .chargeLevel(HIGH_CHARGE_LEVEL)
                           .state(DroneState.IDLE)
                           .active(true)
                           .build();

        droneRepository.save(drone);

        droneService.getBatteryLevel(drone.getId());
        final Optional<Drone> resultDrone = droneRepository.findById(drone.getId());

        assertThat(resultDrone).isNotEmpty();
        assertEquals(resultDrone.get().getChargeLevel(), HIGH_CHARGE_LEVEL);
    }

    @Test
    @DisplayName("Get medication - Ок")
    void getMedication() {

        Drone drone = Drone.builder()
                           .serialNumber(DRONE_SERIAL_NUMBER)
                           .model(DRONE_MODEL)
                           .weight(DRONE_WEIGHT)
                           .chargeLevel(HIGH_CHARGE_LEVEL)
                           .state(DroneState.IDLE)
                           .active(true)
                           .build();

        droneRepository.save(drone);

        Medication medication = Medication.builder()
                                          .name(MEDICATION_NAME_1)
                                          .weight(MEDICATION_WEIGHT_1)
                                          .active(true)
                                          .build();

        medicationRepository.save(medication);

        DroneMedication droneMedication = DroneMedication.builder()
                                                         .droneId(drone.getId())
                                                         .medicationId(medication.getId())
                                                         .build();
        droneMedicationRepository.save(droneMedication);

        List<MedicationDto> result = droneService.getMedication(drone.getId());

        assertNotNull(result);
        assertEquals(result.size(), 1);
    }

    @Test
    @DisplayName("Load medication - Ок")
    void loadMedicationOk() {

        Drone drone = Drone.builder()
                           .serialNumber(DRONE_SERIAL_NUMBER)
                           .model(DRONE_MODEL)
                           .weight(DRONE_WEIGHT)
                           .chargeLevel(HIGH_CHARGE_LEVEL)
                           .state(DroneState.IDLE)
                           .active(true)
                           .build();

        droneRepository.save(drone);

        Medication medication_1 = Medication.builder()
                                            .name(MEDICATION_NAME_1)
                                            .weight(MEDICATION_WEIGHT_1)
                                            .active(true)
                                            .build();

        medicationRepository.save(medication_1);

        Medication medication_2 = Medication.builder()
                                            .name(MEDICATION_NAME_2)
                                            .weight(MEDICATION_WEIGHT_2)
                                            .active(true)
                                            .build();

        medicationRepository.save(medication_2);

        List<Long> medicationIds = Arrays.asList(medication_1.getId(), medication_2.getId());
        String result = droneService.loadMedication(drone.getId(), medicationIds);

        final List<DroneMedication> droneMedication = droneMedicationRepository.findByDroneId(drone.getId());

        assertEquals(result, "Success");
        assertEquals(droneMedication.size(), 2);
    }

    @Test
    @DisplayName("Load medication - Bad request")
    void loadMedicationBadRequest() {

        Drone drone = Drone.builder()
                           .serialNumber(DRONE_SERIAL_NUMBER)
                           .model(DRONE_MODEL)
                           .weight(DRONE_WEIGHT_2)
                           .chargeLevel(HIGH_CHARGE_LEVEL)
                           .state(DroneState.IDLE)
                           .active(true)
                           .build();

        droneRepository.save(drone);

        Medication medication_1 = Medication.builder()
                                            .name(MEDICATION_NAME_1)
                                            .weight(MEDICATION_WEIGHT_1)
                                            .active(true)
                                            .build();

        medicationRepository.save(medication_1);

        Medication medication_2 = Medication.builder()
                                            .name(MEDICATION_NAME_2)
                                            .weight(MEDICATION_WEIGHT_2)
                                            .active(true)
                                            .build();

        medicationRepository.save(medication_2);

        List<Long> medicationIds = Arrays.asList(medication_1.getId(), medication_2.getId());

        assertThrows(BadRequestException.class, () -> droneService.loadMedication(drone.getId(), medicationIds));
    }

    @Test
    @DisplayName("Get active drones")
    void getAllActiveDrones() {

        Drone drone_1 = Drone.builder()
                             .serialNumber(DRONE_SERIAL_NUMBER)
                             .model(DRONE_MODEL)
                             .weight(DRONE_WEIGHT)
                             .active(true)
                             .build();

        droneRepository.save(drone_1);

        Drone drone_2 = Drone.builder()
                             .serialNumber(DRONE_SERIAL_NUMBER_2)
                             .model(DRONE_MODEL_2)
                             .weight(DRONE_WEIGHT_2)
                             .active(true)
                             .build();

        droneRepository.save(drone_2);

        Drone drone_3 = Drone.builder()
                             .serialNumber(DRONE_SERIAL_NUMBER_3)
                             .model(DRONE_MODEL_3)
                             .weight(DRONE_WEIGHT_3)
                             .active(false)
                             .build();

        droneRepository.save(drone_3);

        final List<DroneDto> droneDtoList = droneService.getAllActiveDrones();
        assertNotNull(droneDtoList);
        assertEquals(droneDtoList.size(), 2);
    }

    @Test
    @DisplayName("Delete drone")
    void deleteDrone() {

        Drone drone = Drone.builder()
                           .serialNumber(DRONE_SERIAL_NUMBER)
                           .model(DRONE_MODEL)
                           .weight(DRONE_WEIGHT)
                           .active(true)
                           .build();

        droneRepository.save(drone);

        droneService.deleteDrone(drone.getId());
        final Optional<Drone> resultDrone = droneRepository.findById(drone.getId());

        assertThat(resultDrone).isNotEmpty();
        assertEquals(resultDrone.get().getActive(), false);
    }
}