package com.nemtsov.drones.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nemtsov.drones.dto.DroneDto;
import com.nemtsov.drones.dto.MedicationDto;
import com.nemtsov.drones.enums.DroneModel;
import com.nemtsov.drones.exception.BadRequestException;
import com.nemtsov.drones.exception.NotFoundException;
import com.nemtsov.drones.service.DroneService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author nemtsov free11@list.ru
 */
@WebMvcTest(DroneController.class)
@AutoConfigureMockMvc
class DroneControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    DroneService droneService;

    private static final Long DRONE_ID = 1L;
    private static final String DRONE_SERIAL_NUMBER = "7836482136493295792454";
    private static final DroneModel DRONE_MODEL = DroneModel.LIGHTWEIGHT;
    private static final Short DRONE_WEIGHT = 100;
    private static final Long DRONE_ID_2 = 2L;
    private static final String DRONE_SERIAL_NUMBER_2 = "6402830475493295792432";
    private static final DroneModel DRONE_MODEL_2 = DroneModel.MIDDLEWEIGHT;
    private static final Short DRONE_WEIGHT_2 = 200;
    private static final Short BATTERY_LEVEL = 100;
    private static final List<Long> MEDICATION_ID_LIST = Arrays.asList(1L, 2L, 3L);
    private static final String MEDICATION_NAME = "Medication_1";
    private static final Short MEDICATION_WEIGHT = 100;

    private DroneDto droneDto;
    private List<DroneDto> droneDtoList;

    @BeforeEach
    void setUp() {

        droneDto = DroneDto.builder()
                           .id(DRONE_ID)
                           .serialNumber(DRONE_SERIAL_NUMBER)
                           .model(DRONE_MODEL)
                           .weight(DRONE_WEIGHT)
                           .build();

        DroneDto droneDto2 = DroneDto.builder()
                                     .id(DRONE_ID_2)
                                     .serialNumber(DRONE_SERIAL_NUMBER_2)
                                     .model(DRONE_MODEL_2)
                                     .weight(DRONE_WEIGHT_2)
                                     .build();

        droneDtoList = Arrays.asList(droneDto, droneDto2);
    }

    @Test
    @DisplayName("Register drone - Ok")
    void registerDroneOk() throws Exception {

        Mockito.when(droneService.registerDrone(any()))
               .thenReturn(droneDto);

        mockMvc.perform(post("/api/drones")
                       .content(objectMapper.writeValueAsString(droneDto))
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id").value(droneDto.getId()))
               .andExpect(jsonPath("$.serialNumber").value(droneDto.getSerialNumber()))
               .andExpect(jsonPath("$.model").value(droneDto.getModel().toString()))
               .andExpect(jsonPath("$.weight").value(droneDto.getWeight().toString()));
    }

    @Test
    @DisplayName("Register drone - Bad request")
    void registerDroneBadRequest() throws Exception {

        Mockito.when(droneService.registerDrone(any()))
               .thenThrow(new BadRequestException("Bad request"));

        mockMvc.perform(post("/api/drones")
                       .content(objectMapper.writeValueAsString(droneDto))
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Get available drones - Ok")
    void availableDronesOk() throws Exception {

        Mockito.when(droneService.availableDrones())
               .thenReturn(droneDtoList);

        mockMvc.perform(get("/api/drones/available")
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isOk())
               .andExpect(content().json(objectMapper.writeValueAsString(droneDtoList)));
    }

    @Test
    @DisplayName("Get battery level - Ok")
    void getBatteryLevelOk() throws Exception {

        Mockito.when(droneService.getBatteryLevel(DRONE_ID)).thenReturn(BATTERY_LEVEL);

        mockMvc.perform(get("/api/drones/{droneId}/battery", DRONE_ID)
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isOk())
               .andExpect(content().string(String.valueOf(BATTERY_LEVEL)));
    }

    @Test
    @DisplayName("Get battery level - Not found")
    void getBatteryLevelNotFound() throws Exception {

        Mockito.when(droneService.getBatteryLevel(DRONE_ID))
               .thenThrow(new NotFoundException("Drone not found"));

        mockMvc.perform(get("/api/drones/{droneId}/battery", DRONE_ID)
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Get medications - Ok")
    void getMedicationsOk() throws Exception {

        MedicationDto medicationDto = MedicationDto.builder()
                                                   .name(MEDICATION_NAME)
                                                   .weight(MEDICATION_WEIGHT)
                                                   .build();

        List<MedicationDto> medicationDtoList = Collections.singletonList(medicationDto);

        Mockito.when(droneService.getMedication(DRONE_ID)).thenReturn(medicationDtoList);

        mockMvc.perform(get("/api/drones/{droneId}/medication", DRONE_ID)
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isOk())
               .andExpect(content().json(objectMapper.writeValueAsString(medicationDtoList)));
    }

    @Test
    @DisplayName("Get medications - Not found")
    void getMedicationsNotFound() throws Exception {

        Mockito.when(droneService.getMedication(DRONE_ID))
               .thenThrow(new NotFoundException("Drone not found"));

        mockMvc.perform(get("/api/drones/{droneId}/medication", DRONE_ID)
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Load medication - Ok")
    void loadMedicationOk() throws Exception {

        Mockito.when(droneService.loadMedication(DRONE_ID, MEDICATION_ID_LIST)).thenReturn("Success");

        mockMvc.perform(post("/api/drones/{droneId}/load", DRONE_ID)
                       .content(objectMapper.writeValueAsString(MEDICATION_ID_LIST))
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isOk())
               .andExpect(content().string("Success"));
    }

    @Test
    @DisplayName("Load medication - Not found")
    void loadMedicationNotFound() throws Exception {

        Mockito.when(droneService.loadMedication(DRONE_ID, MEDICATION_ID_LIST))
               .thenThrow(new NotFoundException("Drone not found"));

        mockMvc.perform(post("/api/drones/{droneId}/load", DRONE_ID)
                       .content(objectMapper.writeValueAsString(MEDICATION_ID_LIST))
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Load medication - Bad request")
    void loadMedicationBadRequest() throws Exception {

        Mockito.when(droneService.loadMedication(DRONE_ID, MEDICATION_ID_LIST))
               .thenThrow(new BadRequestException("Bad request"));

        mockMvc.perform(post("/api/drones/{droneId}/load", DRONE_ID)
                       .content(objectMapper.writeValueAsString(MEDICATION_ID_LIST))
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Get all active drones - Ok")
    void getAllActiveDronesOk() throws Exception {

        Mockito.when(droneService.getAllActiveDrones())
               .thenReturn(droneDtoList);

        mockMvc.perform(get("/api/drones")
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isOk())
               .andExpect(content().json(objectMapper.writeValueAsString(droneDtoList)));
    }

    @Test
    @DisplayName("Delete drone - Ok")
    void deleteDroneOk() throws Exception {

        Mockito.when(droneService.deleteDrone(DRONE_ID)).thenReturn("Success");

        mockMvc.perform(delete("/api/drones/{droneId}", DRONE_ID)
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isOk())
               .andExpect(content().string("Success"));
    }

    @Test
    @DisplayName("Delete drone - Not found")
    void deleteDroneNotFound() throws Exception {

        Mockito.when(droneService.deleteDrone(DRONE_ID))
               .thenThrow(new NotFoundException("Drone not found"));

        mockMvc.perform(delete("/api/drones/{droneId}", DRONE_ID)
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isNotFound());
    }
}