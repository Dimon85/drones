package com.nemtsov.drones.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nemtsov.drones.dto.MedicationDto;
import com.nemtsov.drones.exception.NotFoundException;
import com.nemtsov.drones.service.MedicationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

/**
 * @author nemtsov free11@list.ru
 */
@WebMvcTest(MedicationController.class)
@AutoConfigureMockMvc
class MedicationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    MedicationService medicationService;

    private static final Long MEDICATION_ID = 1L;
    private static final String MEDICATION_NAME_1 = "Medication_1";
    private static final Short MEDICATION_WEIGHT_1 = 100;
    private static final String MEDICATION_CODE_1 = "Med_1";
    private static final String MEDICATION_IMAGE_URL_1 = "Med_URL_1";
    private static final String MEDICATION_NAME_2 = "Medication_2";
    private static final Short MEDICATION_WEIGHT_2 = 200;
    private static final String MEDICATION_CODE_2 = "Med_2";
    private static final String MEDICATION_IMAGE_URL_2 = "Med_URL_2";

    private MedicationDto medicationDto;
    private List<MedicationDto> medicationDtoList;

    @BeforeEach
    void setUp() {

        medicationDto = MedicationDto.builder()
                                     .name(MEDICATION_NAME_1)
                                     .weight(MEDICATION_WEIGHT_1)
                                     .code(MEDICATION_CODE_1)
                                     .imageUrl(MEDICATION_IMAGE_URL_1)
                                     .build();

        MedicationDto medicationDto2 = MedicationDto.builder()
                                                    .name(MEDICATION_NAME_2)
                                                    .weight(MEDICATION_WEIGHT_2)
                                                    .code(MEDICATION_CODE_2)
                                                    .imageUrl(MEDICATION_IMAGE_URL_2)
                                                    .build();

        medicationDtoList = Arrays.asList(medicationDto, medicationDto2);
    }

    @Test
    @DisplayName("Add medication - Ok")
    void addMedicationOk() throws Exception {

        Mockito.when(medicationService.addMedication(any()))
               .thenReturn(medicationDto);

        mockMvc.perform(post("/api/medications")
                       .content(objectMapper.writeValueAsString(medicationDto))
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id").value(medicationDto.getId()))
               .andExpect(jsonPath("$.name").value(medicationDto.getName()))
               .andExpect(jsonPath("$.weight").value(medicationDto.getWeight().toString()))
               .andExpect(jsonPath("$.code").value(medicationDto.getCode()))
               .andExpect(jsonPath("$.imageUrl").value(medicationDto.getImageUrl()));
    }

    @Test
    @DisplayName("Get all active medications - Ok")
    void getAllMedicationsOk() throws Exception {

        Mockito.when(medicationService.getAllActiveMedications())
               .thenReturn(medicationDtoList);

        mockMvc.perform(get("/api/medications")
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isOk())
               .andExpect(content().json(objectMapper.writeValueAsString(medicationDtoList)));
    }

    @Test
    @DisplayName("Delete medication - Ok")
    void deleteMedicationOk() throws Exception {

        Mockito.when(medicationService.deleteMedication(MEDICATION_ID)).thenReturn("Success");

        mockMvc.perform(delete("/api/medications/{medicationId}", MEDICATION_ID)
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isOk())
               .andExpect(content().string("Success"));
    }

    @Test
    @DisplayName("Delete medication - Not found")
    void deleteMedicationNotFound() throws Exception {

        Mockito.when(medicationService.deleteMedication(MEDICATION_ID))
               .thenThrow(new NotFoundException("Drone not found"));

        mockMvc.perform(delete("/api/medications/{medicationId}", MEDICATION_ID)
                       .contentType(APPLICATION_JSON_VALUE)
                       .accept(APPLICATION_JSON_VALUE)
               )
               .andExpect(status().isNotFound());
    }
}