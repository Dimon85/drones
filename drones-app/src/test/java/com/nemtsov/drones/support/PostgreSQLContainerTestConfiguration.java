package com.nemtsov.drones.support;

import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

/**
 * Configuration <strong>Postgres</strong> Test Container.
 *
 * @author nemtsov free11@list.ru
 */
@Slf4j
@Configuration
@EntityScan(basePackages = "com.nemtsov.drones.entity")
@EnableJpaRepositories(basePackages = "com.nemtsov.drones.repository")
@EnableJpaAuditing
public class PostgreSQLContainerTestConfiguration {

    protected static final String DEFAULT_USERNAME = "admin";
    protected static final String DEFAULT_PASSWORD = "admin";
    protected static final String DEFAULT_DATABASE_NAME = "drones_db";
    protected static final String DOCKER_POSTGRESQL_IMAGE_NAME = "postgres:12";
    protected static final String DEFAULT_DB = "postgres";

    @Bean
    @SneakyThrows
    public PostgreSQLContainer postgreSQLContainer() {

        DockerImageName dockerImageName = DockerImageName.parse(DOCKER_POSTGRESQL_IMAGE_NAME)
                                                         .asCompatibleSubstituteFor(DEFAULT_DB);

        PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer(dockerImageName);
        postgreSQLContainer
                .withDatabaseName(DEFAULT_DATABASE_NAME)
                .withUsername(DEFAULT_USERNAME)
                .withPassword(DEFAULT_PASSWORD)
                .start();

        return postgreSQLContainer;
    }

    @Bean
    public HikariDataSource dataSource(final PostgreSQLContainer postgreSQLContainer) {

        HikariDataSource properties = new HikariDataSource();
        properties.setJdbcUrl(postgreSQLContainer.getJdbcUrl());
        properties.setUsername(postgreSQLContainer.getUsername());
        properties.setPassword(postgreSQLContainer.getPassword());
        properties.setDriverClassName(postgreSQLContainer.getDriverClassName());

        return properties;
    }
}
