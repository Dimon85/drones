package com.nemtsov.drones.controller;

import static org.springframework.http.ResponseEntity.ok;

import com.nemtsov.drones.api.DroneApi;
import com.nemtsov.drones.dto.DroneDto;
import com.nemtsov.drones.dto.MedicationDto;
import com.nemtsov.drones.service.DroneService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller for Drones.
 *
 * @author nemtsov free11@list.ru
 */
@RestController
@Slf4j
@RequiredArgsConstructor
public class DroneController implements DroneApi {

    private final DroneService droneService;

    @Override
    public ResponseEntity<DroneDto> registerDrone(DroneDto droneDto) {

        return ok(droneService.registerDrone(droneDto));
    }

    @Override
    public ResponseEntity<List<DroneDto>> availableDrones() {

        return ok(droneService.availableDrones());
    }

    @Override
    public ResponseEntity<Short> getBatteryLevel(Long droneId) {

        return ok(droneService.getBatteryLevel(droneId));
    }

    @Override
    public ResponseEntity<List<MedicationDto>> getMedications(Long droneId) {

        return ok(droneService.getMedication(droneId));
    }

    @Override
    public ResponseEntity<String> loadMedication(Long droneId, List<Long> medicationIds) {

        return ok(droneService.loadMedication(droneId, medicationIds));
    }

    @Override
    public ResponseEntity<List<DroneDto>> getAllActiveDrones() {

        return ok(droneService.getAllActiveDrones());
    }

    @Override
    public ResponseEntity<String> deleteDrone(Long droneId) {

        return ok(droneService.deleteDrone(droneId));
    }
}

