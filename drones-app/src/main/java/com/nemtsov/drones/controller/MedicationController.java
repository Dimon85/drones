package com.nemtsov.drones.controller;

import static org.springframework.http.ResponseEntity.ok;

import com.nemtsov.drones.api.MedicationApi;
import com.nemtsov.drones.dto.MedicationDto;
import com.nemtsov.drones.service.MedicationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller for Drones.
 *
 * @author nemtsov free11@list.ru
 */
@RestController
@Slf4j
@RequiredArgsConstructor
public class MedicationController implements MedicationApi {

    private final MedicationService medicationService;

    @Override
    public ResponseEntity<MedicationDto> addMedication(MedicationDto medicationDto) {

        return ok(medicationService.addMedication(medicationDto));
    }

    @Override
    public ResponseEntity<List<MedicationDto>> getAllMedications() {

        return ok(medicationService.getAllActiveMedications());
    }

    @Override
    public ResponseEntity<String> deleteMedication(Long medicationId) {

        return ok(medicationService.deleteMedication(medicationId));
    }
}

