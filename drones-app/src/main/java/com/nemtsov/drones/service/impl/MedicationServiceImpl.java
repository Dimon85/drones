package com.nemtsov.drones.service.impl;

import static java.util.Objects.isNull;

import com.nemtsov.drones.dto.MedicationDto;
import com.nemtsov.drones.entity.Medication;
import com.nemtsov.drones.exception.NotFoundException;
import com.nemtsov.drones.mapper.MedicationMapper;
import com.nemtsov.drones.repository.MedicationRepository;
import com.nemtsov.drones.service.MedicationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link MedicationService}.
 *
 * @author nemtsov free11@list.ru
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class MedicationServiceImpl implements MedicationService {

    private final MedicationMapper medicationMapper;
    private final MedicationRepository medicationRepository;

    @Override
    @Transactional
    public MedicationDto addMedication(MedicationDto medicationDto) {

        //default active status = true
        if (isNull(medicationDto.getActive())) {
            medicationDto.setActive(true);
        }

        Medication medication = medicationRepository.save(medicationMapper.toEntity(medicationDto));

        return medicationMapper.toDto(medication);
    }

    @Override
    public List<MedicationDto> getAllActiveMedications() {

        return medicationMapper.toDtoList(medicationRepository.findAllByActiveIsTrue());
    }

    @Override
    public String deleteMedication(Long medicationId) {

        Medication medication = medicationRepository.findById(medicationId)
                                                    .orElseThrow(() ->
                                                            new NotFoundException(String.format("Medication with id = %s not found", medicationId))
                                                    );

        //set active status
        medication.setActive(false);
        medicationRepository.save(medication);

        return "Success";
    }
}
