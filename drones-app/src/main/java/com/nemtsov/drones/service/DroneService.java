package com.nemtsov.drones.service;

import com.nemtsov.drones.dto.DroneDto;
import com.nemtsov.drones.dto.MedicationDto;

import java.util.List;

/**
 * Drone service.
 *
 * @author nemtsov free11@list.ru
 */
public interface DroneService {

    /**
     * Method registers new drone.
     *
     * @param droneDto request drone data
     * @return saved drone data
     */
    DroneDto registerDrone(DroneDto droneDto);

    /**
     * Method returns ist of drones available for loading
     *
     * @return list of drones
     */
    List<DroneDto> availableDrones();

    /**
     * Method returns battery level for drone.
     *
     * @param droneId drone id
     * @return battery level
     */
    Short getBatteryLevel(Long droneId);

    /**
     * Method returns list of medications loaded onto the drone.
     *
     * @param droneId drone id
     * @return list of medications
     */
    List<MedicationDto> getMedication(Long droneId);

    /**
     * Method loads drone with medications.
     *
     * @param droneId       drone id
     * @param medicationIds list of medication ids
     * @return Success
     */
    String loadMedication(Long droneId, List<Long> medicationIds);

    /**
     * Method returns list of all active drones.
     *
     * @return list of active drones
     */
    List<DroneDto> getAllActiveDrones();

    /**
     * Method mark drone as active = false.
     *
     * @param droneId drone id
     * @return Success
     */
    String deleteDrone(Long droneId);
}
