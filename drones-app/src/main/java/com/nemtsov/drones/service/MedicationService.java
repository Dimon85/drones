package com.nemtsov.drones.service;

import com.nemtsov.drones.dto.MedicationDto;

import java.util.List;

/**
 * Medication service.
 *
 * @author nemtsov free11@list.ru
 */
public interface MedicationService {

    /**
     * Method registers new medication.
     *
     * @param medicationDto request medication data
     * @return saved medication data
     */
    MedicationDto addMedication(MedicationDto medicationDto);

    /**
     * Method returns list of all active medications.
     *
     * @return list of active medications
     */
    List<MedicationDto> getAllActiveMedications();

    /**
     * Method mark medication as active = false.
     *
     * @param medicationId medication id
     * @return Success
     */
    String deleteMedication(Long medicationId);
}
