package com.nemtsov.drones.service.impl;

import static java.util.Objects.isNull;

import com.nemtsov.drones.dto.DroneDto;
import com.nemtsov.drones.dto.MedicationDto;
import com.nemtsov.drones.entity.ChargeLevelLog;
import com.nemtsov.drones.entity.Drone;
import com.nemtsov.drones.entity.DroneMedication;
import com.nemtsov.drones.entity.DroneStateLog;
import com.nemtsov.drones.entity.Medication;
import com.nemtsov.drones.enums.DroneState;
import com.nemtsov.drones.exception.BadRequestException;
import com.nemtsov.drones.exception.NotFoundException;
import com.nemtsov.drones.mapper.DroneMapper;
import com.nemtsov.drones.mapper.MedicationMapper;
import com.nemtsov.drones.repository.ChargeLevelLogRepository;
import com.nemtsov.drones.repository.DroneMedicationRepository;
import com.nemtsov.drones.repository.DroneRepository;
import com.nemtsov.drones.repository.DroneStateLogRepository;
import com.nemtsov.drones.repository.MedicationRepository;
import com.nemtsov.drones.service.DroneService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Implementation of {@link DroneService}.
 *
 * @author nemtsov free11@list.ru
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService {

    private final static String LOG_TAG = "[DRONE_SERVICE_IMPL] : ";
    private final static Short CHARGE_THRESHOLD = 25;
    private final static Short DEFAULT_CHARGE_LEVEL = 100;

    private final ChargeLevelLogRepository chargeLevelLogRepository;
    private final DroneMapper droneMapper;
    private final DroneMedicationRepository droneMedicationRepository;
    private final DroneRepository droneRepository;
    private final DroneStateLogRepository droneStateLogRepository;
    private final MedicationMapper medicationMapper;
    private final MedicationRepository medicationRepository;

    @Override
    @Transactional
    public DroneDto registerDrone(DroneDto droneDto) {

        if (droneRepository.findBySerialNumber(droneDto.getSerialNumber()).isPresent()) {
            throw new BadRequestException("This serial number is already used");
        }

        //default active status = true
        if (isNull(droneDto.getActive())) {
            droneDto.setActive(true);
        }

        Drone drone = droneMapper.toEntity(droneDto);

        //set charge level
        final Short chargeLevel = isNull(droneDto.getChargeLevel())
                ? DEFAULT_CHARGE_LEVEL
                : droneDto.getChargeLevel();

        drone.setChargeLevel(chargeLevel);

        //set IDLE state
        drone.setState(DroneState.IDLE);

        droneRepository.save(drone);

        chargeLevelLogRepository.save(ChargeLevelLog.builder()
                                                    .droneId(drone.getId())
                                                    .chargeLevel(chargeLevel)
                                                    .logTime(LocalDateTime.now())
                                                    .build()
        );

        droneStateLogRepository.save(DroneStateLog.builder()
                                                  .droneId(drone.getId())
                                                  .state(DroneState.IDLE)
                                                  .logTime(LocalDateTime.now())
                                                  .build()
        );

        log.info("{} drone has been registered [id = {}]", LOG_TAG, drone.getId());

        return droneMapper.toDto(drone);
    }

    @Override
    public List<DroneDto> availableDrones() {

        return droneMapper.toDtoList(
                droneRepository.findAllByActiveIsTrueAndStateAndChargeLevelGreaterThan(DroneState.IDLE, CHARGE_THRESHOLD)
        );
    }

    @Override
    public Short getBatteryLevel(Long droneId) {

        Drone drone = droneRepository.findById(droneId)
                                     .orElseThrow(() ->
                                             new NotFoundException(String.format("Drone with id = %s not found", droneId))
                                     );

        return drone.getChargeLevel();
    }

    @Override
    public List<MedicationDto> getMedication(Long droneId) {

        if (!droneRepository.existsById(droneId)) {
            throw new NotFoundException(String.format("Drone with id = %s not found", droneId));
        }

        final List<Long> medicationIds = droneMedicationRepository.findByDroneId(droneId).stream()
                                                                  .map(DroneMedication::getMedicationId)
                                                                  .toList();

        return medicationMapper.toDtoList(medicationRepository.findAllByActiveIsTrueAndIdIn(medicationIds));
    }

    @Override
    @Transactional
    public String loadMedication(Long droneId, List<Long> medicationIds) {

        Drone drone = droneRepository.findById(droneId)
                                     .orElseThrow(() ->
                                             new NotFoundException(String.format("Drone with id = %s not found", droneId))
                                     );

        if (drone.getChargeLevel() < CHARGE_THRESHOLD) {
            throw new BadRequestException(
                    String.format("Battery level is below %s. Check available drones on `api/drones/available`", CHARGE_THRESHOLD)
            );
        }

        final List<Medication> medications = medicationRepository.findAllByActiveIsTrueAndIdIn(medicationIds);

        final int totalWeight = medications.stream().mapToInt(Medication::getWeight).sum();

        if (drone.getWeight() < totalWeight) {
            throw new BadRequestException(
                    String.format("You are trying to load %s, but max weight only %s", totalWeight, drone.getWeight())
            );
        }

        medications.forEach(m ->
                droneMedicationRepository.save(DroneMedication.builder()
                                                              .droneId(droneId)
                                                              .medicationId(m.getId())
                                                              .weight(m.getWeight())
                                                              .build()
                )
        );

        //set status
        drone.setState(DroneState.LOADING);
        droneRepository.save(drone);
        droneStateLogRepository.save(DroneStateLog.builder()
                                                  .droneId(drone.getId())
                                                  .state(DroneState.LOADING)
                                                  .logTime(LocalDateTime.now())
                                                  .build()
        );

        log.info("{} drone has been loaded [id = {}]", LOG_TAG, droneId);

        return "Success";
    }

    @Override
    public List<DroneDto> getAllActiveDrones() {

        return droneMapper.toDtoList(droneRepository.findAllByActiveIsTrue());
    }

    @Override
    @Transactional
    public String deleteDrone(Long droneId) {

        Drone drone = droneRepository.findById(droneId)
                                     .orElseThrow(() ->
                                             new NotFoundException(String.format("Drone with id = %s not found", droneId))
                                     );

        //set IDLE state
        droneStateLogRepository.save(DroneStateLog.builder()
                                                  .droneId(droneId)
                                                  .state(DroneState.IDLE)
                                                  .logTime(LocalDateTime.now())
                                                  .build()
        );

        //set active status
        drone.setActive(false);
        droneRepository.save(drone);

        log.info("{} drone has been deleted [id = {}]", LOG_TAG, droneId);

        return "Success";
    }
}
