package com.nemtsov.drones.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Medication JPA entity.
 *
 * @author nemtsov free11@list.ru
 */
@Data
@Entity
@Table(schema = "public", name = "medication")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Medication {

    /**
     * Unique identifier.
     */
    @Id
    @SequenceGenerator(allocationSize = 1, name = "medication_id", schema = "public",
                       sequenceName = "medication_id_seq")
    @GeneratedValue(generator = "medication_id", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * Name.
     */
    @Column(name = "name")
    private String name;

    /**
     * Weight.
     */
    @Column(name = "weight")
    private Short weight;

    /**
     * Code.
     */
    @Column(name = "code")
    private String code;

    /**
     * Image URL.
     */
    @Column(name = "image_url")
    private String imageUrl;

    /**
     * Active.
     */
    @Column(name = "active")
    private Boolean active;
}