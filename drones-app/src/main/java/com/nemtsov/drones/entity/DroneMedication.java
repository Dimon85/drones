package com.nemtsov.drones.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Medications loaded to drone JPA entity.
 *
 * @author nemtsov free11@list.ru
 */
@Data
@Entity
@Table(schema = "public", name = "drone_medication")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DroneMedication {

    /**
     * Unique identifier.
     */
    @Id
    @SequenceGenerator(allocationSize = 1, name = "drone_medication_id", schema = "public",
                       sequenceName = "drone_medication_id_seq")
    @GeneratedValue(generator = "drone_medication_id", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * Drone id.
     */
    @Column(name = "drone_id")
    private Long droneId;

    /**
     * Medication id.
     */
    @Column(name = "medication_id")
    private Long medicationId;

    /**
     * Weight.
     */
    @Column(name = "weight")
    private Short weight;
}