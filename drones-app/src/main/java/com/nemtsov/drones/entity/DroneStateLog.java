package com.nemtsov.drones.entity;

import com.nemtsov.drones.enums.DroneState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Drone state JPA entity.
 *
 * @author nemtsov free11@list.ru
 */
@Data
@Entity
@Table(schema = "public", name = "drone_state_log")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DroneStateLog {

    /**
     * Unique identifier.
     */
    @Id
    @SequenceGenerator(allocationSize = 1, name = "drone_state_log_id", schema = "public",
                       sequenceName = "drone_state_log_id_seq")
    @GeneratedValue(generator = "drone_state_log_id", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * Drone ID.
     */
    @Column(name = "drone_id")
    private Long droneId;

    /**
     * Log time.
     */
    @Column(name = "log_time")
    private LocalDateTime logTime;

    /**
     * Drone state.
     */
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private DroneState state;
}