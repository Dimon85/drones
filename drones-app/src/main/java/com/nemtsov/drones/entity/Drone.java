package com.nemtsov.drones.entity;

import com.nemtsov.drones.enums.DroneModel;
import com.nemtsov.drones.enums.DroneState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Drone JPA entity.
 *
 * @author nemtsov free11@list.ru
 */
@Data
@Entity
@Table(schema = "public", name = "drone")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Drone {

    /**
     * Unique identifier.
     */
    @Id
    @SequenceGenerator(allocationSize = 1, name = "drone_id", schema = "public",
                       sequenceName = "drone_id_seq")
    @GeneratedValue(generator = "drone_id", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * Serial number.
     */
    @Column(name = "serial_number")
    private String serialNumber;

    /**
     * Drone model.
     */
    @Column(name = "model")
    @Enumerated(EnumType.STRING)
    private DroneModel model;

    /**
     * Weight.
     */
    @Column(name = "weight")
    private Short weight;

    /**
     * Charge level.
     */
    @Column(name = "charge_level")
    private Short chargeLevel;

    /**
     * State.
     */
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private DroneState state;

    /**
     * Active.
     */
    @Column(name = "active")
    private Boolean active;
}
