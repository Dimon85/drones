package com.nemtsov.drones.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Drone charge level JPA entity.
 *
 * @author nemtsov free11@list.ru
 */
@Data
@Entity
@Table(schema = "public", name = "charge_level_log")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChargeLevelLog {

    /**
     * Unique identifier.
     */
    @Id
    @SequenceGenerator(allocationSize = 1, name = "charge_level_log_id", schema = "public",
                       sequenceName = "charge_level_log_id_seq")
    @GeneratedValue(generator = "charge_level_log_id", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * Drone ID.
     */
    @Column(name = "drone_id")
    private Long droneId;

    /**
     * Log time.
     */
    @Column(name = "log_time")
    private LocalDateTime logTime;

    /**
     * Charge level.
     */
    @Column(name = "charge_level")
    private Short chargeLevel;
}