package com.nemtsov.drones.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.nemtsov.drones.dto.DroneDto;
import com.nemtsov.drones.entity.Drone;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Mapper for Drone.
 *
 * @author nemtsov free11@list.ru
 */
@Mapper(
        unmappedTargetPolicy = IGNORE,
        componentModel = "spring"
)
public interface DroneMapper {

    /**
     * Map {@link Drone} to {@link DroneDto}.
     */
    DroneDto toDto(Drone entity);

    /**
     * Map {@link DroneDto} to {@link Drone}.
     */
    Drone toEntity(DroneDto dto);

    /**
     * Map {@link Drone} list to {@link DroneDto} list.
     */
    List<DroneDto> toDtoList(List<Drone> entity);

    /**
     * Map {@link DroneDto} list to {@link Drone} list.
     */
    List<Drone> toEntityList(List<DroneDto> dto);
}