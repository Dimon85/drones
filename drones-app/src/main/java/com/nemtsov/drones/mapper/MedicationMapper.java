package com.nemtsov.drones.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.nemtsov.drones.dto.MedicationDto;
import com.nemtsov.drones.entity.Medication;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Mapper for Medication.
 *
 * @author nemtsov free11@list.ru
 */
@Mapper(
        unmappedTargetPolicy = IGNORE,
        componentModel = "spring"
)
public interface MedicationMapper {

    /**
     * Map {@link Medication} to {@link MedicationDto}.
     */
    MedicationDto toDto(Medication entity);

    /**
     * Map {@link MedicationDto} to {@link Medication}.
     */
    Medication toEntity(MedicationDto dto);

    /**
     * Map {@link Medication} list to {@link MedicationDto} list.
     */
    List<MedicationDto> toDtoList(List<Medication> entity);

    /**
     * Map {@link MedicationDto} list to {@link Medication} list.
     */
    List<Medication> toEntityList(List<MedicationDto> dto);
}