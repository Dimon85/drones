package com.nemtsov.drones.provider;

import com.nemtsov.drones.enums.DroneState;

/**
 * Drone request provider.
 * Assumed that we can request the drone for its states, e.g. charge level.
 *
 * @author nemtsov free11@list.ru
 */
public interface DroneRequestProvider {

    /**
     * Returns drone's actual charge level.
     *
     * @param droneId drone id
     * @return charge level
     */
    Short getChargeLevel(Long droneId);

    /**
     * Returns drone's actual state.
     *
     * @param droneId drone id
     * @return state
     */
    DroneState getState(Long droneId);
}
