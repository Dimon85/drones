package com.nemtsov.drones.provider.impl;

import com.nemtsov.drones.entity.ChargeLevelLog;
import com.nemtsov.drones.entity.DroneStateLog;
import com.nemtsov.drones.enums.DroneState;
import com.nemtsov.drones.provider.DroneRequestProvider;
import com.nemtsov.drones.repository.ChargeLevelLogRepository;
import com.nemtsov.drones.repository.DroneStateLogRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Default drone request provider.
 * Mock all drone responses for demonstration.
 * We assume that drone automatically changes it's state every STATE_CHANGE_INTERVAL minutes,
 * only state LOADING sets by using /api/drones/{droneId}/load endpoint.
 * Charge level changes every CHARGE_LEVEL_DECREASE_INTERVAL minutes,
 * it decreases on 1% for IDLE, LOADING, LOADED, DELIVERED states and 5% for DELIVERING, RETURNING states.
 * When it reaches zero, it automatically charges to 100.
 *
 * @author nemtsov free11@list.ru
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class MockDroneRequestProvider implements DroneRequestProvider {

    private static final String LOG_TAG = "[MOCK_CHARGE_LEVEL_PROVIDER] : ";
    private static final int CHARGE_LEVEL_DECREASE_INTERVAL = 5;
    private static final int STATE_CHANGE_INTERVAL = 10;

    private final ChargeLevelLogRepository chargeLevelLogRepository;
    private final DroneStateLogRepository droneStateLogRepository;

    @Override
    public Short getChargeLevel(Long droneId) {

        log.debug("{} getChargeLevel [droneId = {}]", LOG_TAG, droneId);

        short chargeLevel = 0;

        final Optional<ChargeLevelLog> chargeLevelLog = chargeLevelLogRepository.findFirstByDroneIdOrderByLogTimeDesc(droneId);

        if (chargeLevelLog.isPresent()) {
            Optional<DroneStateLog> stateLog = droneStateLogRepository.findFirstByDroneIdOrderByLogTimeDesc(droneId);
            long decrease = 0;
            if (stateLog.isPresent()) {
                decrease = getChargeDecrease(stateLog.get().getState(), chargeLevelLog.get().getLogTime());
            }
            chargeLevel = (short) (chargeLevelLog.get().getChargeLevel() - decrease);

            //if chargeLevel decreased to 0, let's assume that drone automatically charged
            chargeLevel = chargeLevel <= 0 ? 100 : chargeLevel;
        }

        return chargeLevel;
    }

    @Override
    public DroneState getState(Long droneId) {

        log.debug("{} getState [droneId = {}]", LOG_TAG, droneId);

        DroneState state = DroneState.IDLE;

        final Optional<DroneStateLog> droneStateLog = droneStateLogRepository.findFirstByDroneIdOrderByLogTimeDesc(droneId);

        if (droneStateLog.isPresent()) {
            state = getNewState(droneStateLog);
        }

        return state;
    }

    private long getChargeDecrease(DroneState state, LocalDateTime lastLogTime) {

        long minutes = Duration.between(lastLogTime, LocalDateTime.now()).toMinutes();
        final long intervalsBetween = minutes / CHARGE_LEVEL_DECREASE_INTERVAL;

        return switch (state) {
            case IDLE, LOADING, LOADED, DELIVERED -> intervalsBetween;
            case DELIVERING, RETURNING -> 5 * intervalsBetween;
        };
    }

    private DroneState getNewState(Optional<DroneStateLog> droneStateLog) {

        if (droneStateLog.isEmpty() || DroneState.IDLE.equals(droneStateLog.get().getState())) {
            return DroneState.IDLE;
        }

        DroneState state = droneStateLog.get().getState();
        long minutes = Duration.between(droneStateLog.get().getLogTime(), LocalDateTime.now()).toMinutes();
        final long intervalsBetween = minutes / STATE_CHANGE_INTERVAL;

        for (int i = 0; i < intervalsBetween; i++) {
            state = getNextState(state);
        }

        return state;
    }

    private DroneState getNextState(DroneState state) {

        return switch (state) {
            case IDLE, RETURNING -> DroneState.IDLE;
            case LOADING -> DroneState.LOADED;
            case LOADED -> DroneState.DELIVERING;
            case DELIVERING -> DroneState.DELIVERED;
            case DELIVERED -> DroneState.RETURNING;
        };
    }
}
