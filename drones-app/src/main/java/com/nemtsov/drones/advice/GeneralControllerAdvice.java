package com.nemtsov.drones.advice;

import com.nemtsov.drones.dto.ExceptionDto;
import com.nemtsov.drones.exception.BadRequestException;
import com.nemtsov.drones.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;

/**
 * Exceptions handler.
 *
 * @author nemtsov free11@list.ru
 */
@ControllerAdvice(basePackages = "com.nemtsov.drones")
public class GeneralControllerAdvice {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(NotFoundException e) {

        return new ResponseEntity<>(
                ExceptionDto.builder()
                            .message(e.getMessage())
                            .timestamp(LocalDateTime.now())
                            .status(404)
                            .build(),
                HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<Object> handleNotFoundException(BadRequestException e) {

        return new ResponseEntity<>(
                ExceptionDto.builder()
                            .message(e.getMessage())
                            .timestamp(LocalDateTime.now())
                            .status(400)
                            .build(),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintValidationException(ConstraintViolationException e) {

        StringBuffer resultMessage = new StringBuffer();
        e.getConstraintViolations().forEach(violation ->
                resultMessage.append(violation.getMessage()).append("/n")
        );

        return new ResponseEntity<>(
                ExceptionDto.builder()
                            .message(resultMessage.toString())
                            .timestamp(LocalDateTime.now())
                            .status(400)
                            .build(),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {

        StringBuffer resultMessage = new StringBuffer();
        e.getBindingResult().getFieldErrors().forEach(error ->
                resultMessage.append(error.getDefaultMessage()).append(System.getProperty("line.separator"))
        );

        return new ResponseEntity<>(
                ExceptionDto.builder()
                            .message(resultMessage.toString())
                            .timestamp(LocalDateTime.now())
                            .status(400)
                            .build(),
                HttpStatus.BAD_REQUEST
        );
    }
}
