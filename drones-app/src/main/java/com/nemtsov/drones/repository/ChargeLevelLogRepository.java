package com.nemtsov.drones.repository;

import com.nemtsov.drones.entity.ChargeLevelLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Charge level log JPA repository.
 *
 * @author nemtsov free11@list.ru
 */
public interface ChargeLevelLogRepository extends JpaRepository<ChargeLevelLog, Long> {

    Optional<ChargeLevelLog> findFirstByDroneIdOrderByLogTimeDesc(Long droneId);
}
