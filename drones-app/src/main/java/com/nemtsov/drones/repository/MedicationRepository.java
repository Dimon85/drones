package com.nemtsov.drones.repository;

import com.nemtsov.drones.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Medication JPA repository.
 *
 * @author nemtsov free11@list.ru
 */
public interface MedicationRepository extends JpaRepository<Medication, Long> {

    List<Medication> findAllByActiveIsTrue();

    List<Medication> findAllByActiveIsTrueAndIdIn(List<Long> idList);
}
