package com.nemtsov.drones.repository;

import com.nemtsov.drones.entity.DroneStateLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Drone state log JPA repository.
 *
 * @author nemtsov free11@list.ru
 */
public interface DroneStateLogRepository extends JpaRepository<DroneStateLog, Long> {

    Optional<DroneStateLog> findFirstByDroneIdOrderByLogTimeDesc(Long droneId);
}
