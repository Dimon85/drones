package com.nemtsov.drones.repository;

import com.nemtsov.drones.entity.Drone;
import com.nemtsov.drones.enums.DroneState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Drone JPA repository.
 *
 * @author nemtsov free11@list.ru
 */
public interface DroneRepository extends JpaRepository<Drone, Long> {

    Optional<Drone> findBySerialNumber(String serialNumber);

    List<Drone> findAllByActiveIsTrue();

    List<Drone> findAllByActiveIsTrueAndStateAndChargeLevelGreaterThan(DroneState state, Short chargeLevel);
}
