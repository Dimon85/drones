package com.nemtsov.drones.repository;

import com.nemtsov.drones.entity.DroneMedication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Drone-medication JPA repository.
 *
 * @author nemtsov free11@list.ru
 */
public interface DroneMedicationRepository extends JpaRepository<DroneMedication, Long> {

    List<DroneMedication> findByDroneId(Long droneId);

    void deleteAllByDroneId(Long droneId);
}
