package com.nemtsov.drones.scheduler;

import static com.nemtsov.drones.enums.DroneState.RETURNING;

import com.nemtsov.drones.dto.DroneDto;
import com.nemtsov.drones.entity.ChargeLevelLog;
import com.nemtsov.drones.entity.DroneStateLog;
import com.nemtsov.drones.enums.DroneState;
import com.nemtsov.drones.provider.DroneRequestProvider;
import com.nemtsov.drones.repository.ChargeLevelLogRepository;
import com.nemtsov.drones.repository.DroneMedicationRepository;
import com.nemtsov.drones.repository.DroneStateLogRepository;
import com.nemtsov.drones.service.DroneService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Scheduled tasks for drones.
 *
 * @author nemtsov free11@list.ru
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class DroneScheduler {

    private final static String LOG_TAG = "[DRONE_SCHEDULER] : ";

    private final ChargeLevelLogRepository chargeLevelLogRepository;
    private final DroneMedicationRepository droneMedicationRepository;
    private final DroneRequestProvider droneRequestProvider;
    private final DroneService droneService;
    private final DroneStateLogRepository droneStateLogRepository;

    /**
     * Scheduled task for updating drone's states and battery levels.
     */
    @Scheduled(
            fixedRateString = "${scheduling.updateDronesState.fixedRate}",
            initialDelayString = "${scheduling.updateDronesState.initialDelay}"
    )
    @Transactional
    public void updateDronesState() {

        final List<DroneDto> activeDrones = droneService.getAllActiveDrones();

        activeDrones.forEach(drone -> {
            updateChargeLevel(drone.getId());
            updateDroneState(drone.getId());
        });

        log.debug("{} updateDronesState - updated {}",
                LOG_TAG,
                activeDrones.size()
        );
    }

    private void updateChargeLevel(Long droneId) {

        final Short chargeLevel = droneRequestProvider.getChargeLevel(droneId);

        if (chargeLevel != 0) {
            chargeLevelLogRepository.save(ChargeLevelLog.builder()
                                                        .droneId(droneId)
                                                        .chargeLevel(chargeLevel)
                                                        .logTime(LocalDateTime.now())
                                                        .build()
            );
        }
    }

    private void updateDroneState(Long droneId) {

        DroneState currentState = null;
        final Optional<DroneStateLog> droneStateLog = droneStateLogRepository.findFirstByDroneIdOrderByLogTimeDesc(droneId);
        if (droneStateLog.isPresent()) {
            currentState = droneStateLog.get().getState();
        }

        final DroneState newState = droneRequestProvider.getState(droneId);

        //write new state if it has changed
        if (!Objects.equals(currentState, newState)) {
            droneStateLogRepository.save(DroneStateLog.builder()
                                                      .droneId(droneId)
                                                      .state(newState)
                                                      .logTime(LocalDateTime.now())
                                                      .build()
            );

            //clear drone loading list if drone is returning
            if (RETURNING.equals(newState)) {
                droneMedicationRepository.deleteAllByDroneId(droneId);
            }
        }
    }
}
