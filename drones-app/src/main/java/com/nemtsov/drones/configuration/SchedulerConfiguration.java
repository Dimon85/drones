/*
 * Reksoft. Do not reproduce without permission in writing.
 * Copyright (c) 2022 Reksoft. All rights reserved.
 */

package com.nemtsov.drones.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * Scheduler configuration.
 *
 * @author nemtsov free11@list.ru
 */
@Configuration
@EnableScheduling
@Slf4j
public class SchedulerConfiguration implements SchedulingConfigurer {

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {

        ThreadPoolTaskScheduler result = new ThreadPoolTaskScheduler();
        result.setPoolSize(5);
        result.setErrorHandler(t -> log.error("Exception in @Scheduled task. ", t));
        result.setThreadNamePrefix("@scheduled-");
        result.initialize();

        taskRegistrar.setTaskScheduler(result);
    }
}