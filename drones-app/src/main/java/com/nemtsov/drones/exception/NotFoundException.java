package com.nemtsov.drones.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * Exception occurring if we can't find anything.
 *
 * @author nemtsov free11@list.ru
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NotFoundException extends RuntimeException {

    /**
     * serialVersionUID.
     */
    @Serial
    private static final long serialVersionUID = 1767645440249928432L;

    public NotFoundException(String message) {
        super(message);
    }
}



