package com.nemtsov.drones.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * Exception occurring if we can't handle the request.
 *
 * @author nemtsov free11@list.ru
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BadRequestException extends RuntimeException {

    /**
     * serialVersionUID.
     */
    @Serial
    private static final long serialVersionUID = -2640225023162151342L;

    public BadRequestException(String message) {
        super(message);
    }
}



