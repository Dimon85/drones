-- #####################################################################################################################
-- Initial data script.
-- #####################################################################################################################

-- drones
insert into drone (id, serial_number, model, weight, charge_level, state, active) values (1, '0136482136493295792454', 'LIGHTWEIGHT', 100, 100, 'IDLE', true);
insert into drone (id, serial_number, model, weight, charge_level, state, active) values (2, '0236482136493295792454', 'LIGHTWEIGHT', 100, 100, 'IDLE', true);
insert into drone (id, serial_number, model, weight, charge_level, state, active) values (3, '0336482136493295792454', 'LIGHTWEIGHT', 100, 100, 'IDLE', true);
insert into drone (id, serial_number, model, weight, charge_level, state, active) values (4, '0436482136493295792454', 'LIGHTWEIGHT', 100, 100, 'IDLE', true);
insert into drone (id, serial_number, model, weight, charge_level, state, active) values (5, '0536482136493295792454', 'MIDDLEWEIGHT', 300, 100, 'IDLE', true);
insert into drone (id, serial_number, model, weight, charge_level, state, active) values (6, '0636482136493295792454', 'MIDDLEWEIGHT', 300, 100, 'IDLE', true);
insert into drone (id, serial_number, model, weight, charge_level, state, active) values (7, '0736482136493295792454', 'LIGHTWEIGHT', 100, 100, 'IDLE', true);
insert into drone (id, serial_number, model, weight, charge_level, state, active) values (8, '0836482136493295792454', 'LIGHTWEIGHT', 100, 100, 'IDLE', true);
insert into drone (id, serial_number, model, weight, charge_level, state, active) values (9, '0936482136493295792454', 'HEAVYWEIGHT', 500, 100, 'IDLE', true);
insert into drone (id, serial_number, model, weight, charge_level, state, active) values (10, '1036482136493295792454', 'HEAVYWEIGHT', 500, 100, 'IDLE', true);

-- medications
insert into medication (id, name, weight, code, image_url, active) values (1, 'Medication-1', 200, 'MED_1', 'string url', true);
insert into medication (id, name, weight, code, image_url, active) values (2, 'Medication-2', 200, 'MED_2', 'string url', true);
insert into medication (id, name, weight, code, image_url, active) values (3, 'Medication-3', 100, 'MED_3', 'string url', true);
insert into medication (id, name, weight, code, image_url, active) values (4, 'Medication-4', 200, 'MED_4', 'string url', true);


-- charge level log
insert into charge_level_log (id, drone_id, log_time, charge_level) values (1, 1, '2023-01-09 10:21:36.918517', 100);
insert into charge_level_log (id, drone_id, log_time, charge_level) values (2, 2, '2023-01-09 10:21:36.918517', 100);
insert into charge_level_log (id, drone_id, log_time, charge_level) values (3, 3, '2023-01-09 10:21:36.918517', 100);
insert into charge_level_log (id, drone_id, log_time, charge_level) values (4, 4, '2023-01-09 10:21:36.918517', 100);
insert into charge_level_log (id, drone_id, log_time, charge_level) values (5, 5, '2023-01-09 10:21:36.918517', 100);
insert into charge_level_log (id, drone_id, log_time, charge_level) values (6, 6, '2023-01-09 10:21:36.918517', 100);
insert into charge_level_log (id, drone_id, log_time, charge_level) values (7, 7, '2023-01-09 10:21:36.918517', 100);
insert into charge_level_log (id, drone_id, log_time, charge_level) values (8, 8, '2023-01-09 10:21:36.918517', 100);
insert into charge_level_log (id, drone_id, log_time, charge_level) values (9, 9, '2023-01-09 10:21:36.918517', 100);
insert into charge_level_log (id, drone_id, log_time, charge_level) values (10, 10, '2023-01-09 10:21:36.918517', 100);


insert into drone_state_log (id, drone_id, log_time, state) values (1, 1, '2023-01-09 10:21:36.925545', 'IDLE');
insert into drone_state_log (id, drone_id, log_time, state) values (2, 2, '2023-01-09 10:21:36.925545', 'IDLE');
insert into drone_state_log (id, drone_id, log_time, state) values (3, 3, '2023-01-09 10:21:36.925545', 'IDLE');
insert into drone_state_log (id, drone_id, log_time, state) values (4, 4, '2023-01-09 10:21:36.925545', 'IDLE');
insert into drone_state_log (id, drone_id, log_time, state) values (5, 5, '2023-01-09 10:21:36.925545', 'IDLE');
insert into drone_state_log (id, drone_id, log_time, state) values (6, 6, '2023-01-09 10:21:36.925545', 'IDLE');
insert into drone_state_log (id, drone_id, log_time, state) values (7, 7, '2023-01-09 10:21:36.925545', 'IDLE');
insert into drone_state_log (id, drone_id, log_time, state) values (8, 8, '2023-01-09 10:21:36.925545', 'IDLE');
insert into drone_state_log (id, drone_id, log_time, state) values (9, 9, '2023-01-09 10:21:36.925545', 'IDLE');
insert into drone_state_log (id, drone_id, log_time, state) values (10, 10, '2023-01-09 10:21:36.925545', 'IDLE');