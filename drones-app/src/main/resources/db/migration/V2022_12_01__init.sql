-- #####################################################################################################################
-- Initial script.
-- #####################################################################################################################

CREATE TABLE drone
(
    id            BIGINT PRIMARY KEY,
    serial_number VARCHAR(100),
    model         VARCHAR,
    weight        SMALLINT,
    charge_level  SMALLINT,
    state         VARCHAR,
    active        BOOLEAN
);

CREATE SEQUENCE drone_id_seq START 1;

CREATE TABLE medication
(
    id        BIGINT PRIMARY KEY,
    name      VARCHAR,
    weight    SMALLINT,
    code      VARCHAR,
    image_url VARCHAR,
    active    BOOLEAN
);

CREATE SEQUENCE medication_id_seq START 1;

CREATE TABLE drone_medication
(
    id            BIGINT PRIMARY KEY,
    drone_id      BIGINT
        CONSTRAINT drone_medication_drone_id_fk
            REFERENCES drone,
    medication_id BIGINT
        CONSTRAINT drone_medication_medication_id_fk
            REFERENCES medication,
    weight        SMALLINT
);

CREATE SEQUENCE drone_medication_id_seq START 1;

CREATE TABLE charge_level_log
(
    id           BIGINT PRIMARY KEY,
    drone_id     BIGINT
        CONSTRAINT drone_medication_drone_id_fk
            REFERENCES drone,
    log_time     TIMESTAMP,
    charge_level SMALLINT
);

CREATE SEQUENCE charge_level_log_id_seq START 1;

CREATE TABLE drone_state_log
(
    id       BIGINT PRIMARY KEY,
    drone_id BIGINT
        CONSTRAINT drone_medication_drone_id_fk
            REFERENCES drone,
    log_time TIMESTAMP,
    state    VARCHAR
);

CREATE SEQUENCE drone_state_log_id_seq START 1;