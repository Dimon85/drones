# Run the application

To run successfully, you need to have java 17 and Docker installed on your computer.

* Launch the docker container with the PostgreSQL database required for the application to work. You can do this with
  the **docker-compose up -d drones_db** command from the project folder. By default the container requires port
  5432 for work.

* Build and run the project. You can:
    * run **mvn clean package** in the project folder, then run the application with the **java -jar ./drones-app/target/drones-app.jar** command
    * or build and run it using your IDE, for example Intelij Idea.

If all the steps have been completed, then you will get a working application on port 8080. You can check the status of the application with a request http://localhost:8080/drones/actuator/health for example from your browser

# Use the application

You can use the app API at http://localhost:8080/drones/swagger-ui/index.html#/